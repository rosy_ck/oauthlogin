package com.tflogin.oauthlogin.Entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.jetbrains.annotations.NotNull;
import javax.persistence.*;

@Entity
@Getter  @Setter  @NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    String  name;
    String surname;

    @NotNull
    String email;
    String telephone;
    String birthDate;
    String fiscalCode;
    //Address come unica stringa o come campi separati?
    String address;
    String civicNumber;
    String city;
    String profileImg;
    String role;

    @OneToOne @Nullable
    @JoinColumn(name = "shop_id", nullable = true, referencedColumnName = "id")
    private Shop userShop;

    public Shop getUserShop() {
        return userShop;
    }

    public void setUserShop(Shop userShop) {
        this.userShop = userShop;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", email='" + email + '\'' +
                ", telephone='" + telephone + '\'' +
                ", birthDate='" + birthDate + '\'' +
                ", fiscalCode='" + fiscalCode + '\'' +
                ", address='" + address + '\'' +
                ", civicNumber='" + civicNumber + '\'' +
                ", city='" + city + '\'' +
                ", profileImg='" + profileImg + '\'' +
                '}';
    }
}


