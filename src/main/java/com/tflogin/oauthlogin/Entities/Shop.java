package com.tflogin.oauthlogin.Entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Shop {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    String name;
    String description;
    String address;
    String civicNumber;
    String city;
    Boolean hasDelivery;
    Boolean hasTakeAway;
    String shopImg; //Path nel db?
    Double longitude;
    Double latitude;
    //List<Forniture> fornitures;

    @Override
    public String toString() {
        return "Shop{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description'"+description+'\'' +
                ", address='" + address + '\'' +
                ", civicNumber='" + civicNumber + '\'' +
                ", city='" + city + '\'' +
                ", hasDelivery=" + hasDelivery +
                ", hasTakeAway=" + hasTakeAway +
                ", shopImg='" + shopImg + '\'' +
                ", longitude='" + longitude + '\'' +
                ", latitude='" + latitude + '\'' +
                '}';
    }
}
