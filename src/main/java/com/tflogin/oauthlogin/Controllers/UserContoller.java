package com.tflogin.oauthlogin.Controllers;

import com.tflogin.oauthlogin.Entities.User;
import com.tflogin.oauthlogin.Repositories.IUserRepository;
import net.minidev.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.Date;

@RestController
//@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1/user")
public class UserContoller {

    private IUserRepository iUserRepository;

    @Autowired
    public UserContoller(IUserRepository iUserRepository) {
        this.iUserRepository = iUserRepository;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<String> insertUser(@RequestBody User b) {
        if (iUserRepository.findByEmailAddress(b.getEmail()) == null) {
            iUserRepository.save(b);
            return new ResponseEntity<>("new user", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("already exists", HttpStatus.OK);
        }
    }


    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public User updateUser(@RequestBody User b) {
        User temp = iUserRepository.findByEmailAddress(b.getEmail());
        if (temp != null) {
            iUserRepository.updateUser(b.getTelephone(),b.getRole(),b.getBirthDate(),b.getFiscalCode(),b.getAddress(),b.getCivicNumber(), b.getCity(),b.getId());
            return iUserRepository.findById(b.getId()).get();
        } else {
            iUserRepository.save(b);
            return iUserRepository.findById(b.getId()).get();
        }
    }


    @RequestMapping(value = "/find", method = RequestMethod.POST)
    public User findUserByEmail(@RequestBody String userEmail) {
        return iUserRepository.findByEmailAddress(userEmail);

    }
}
