package com.tflogin.oauthlogin.Controllers;

import com.tflogin.oauthlogin.Entities.Shop;
import com.tflogin.oauthlogin.Entities.User;
import com.tflogin.oauthlogin.Repositories.IFarmerRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
// @CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1/farmer")
public class FarmerContoller {
    private IFarmerRepositories iFarmerRepository;


    @Autowired
    public FarmerContoller(IFarmerRepositories iFarmerRepository) {
        this.iFarmerRepository = iFarmerRepository;
    }


    @RequestMapping(method = RequestMethod.GET)
    public List<User> findAll(){
        return iFarmerRepository.findAllFarmer();
    }
}
