package com.tflogin.oauthlogin.Controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.tflogin.oauthlogin.Entities.Shop;
import com.tflogin.oauthlogin.Repositories.IShopRepository;
import com.tflogin.oauthlogin.Repositories.IUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
// @CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class ShopController {
    IShopRepository iShopRepository;
    IUserRepository iUserRepository;

    @Autowired
    public ShopController(IShopRepository iShopRepository, IUserRepository iUserRepository) {
        this.iShopRepository = iShopRepository;
        this.iUserRepository = iUserRepository;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/shops")
    public List<Shop> findAllShop(){
        return iShopRepository.findAll();
    }

    @RequestMapping(method = RequestMethod.POST, value="/shop")
    public Shop insertShop(@RequestBody ObjectNode objectNode){
        Shop s;
        String userEmail;

        try {
            userEmail = objectNode.get("userEmail").asText();
            s = new ObjectMapper().convertValue(objectNode.get("Shop"), Shop.class);

            if(iUserRepository.findByEmailAddress(userEmail)!=null){
                Shop snew = iShopRepository.save(s);
                iUserRepository.updateShop(snew, userEmail);
                return snew;
            }
        }catch(Exception ex){
            return null;
        }

        return null;
    }


    @RequestMapping(method = RequestMethod.POST, value="/shop/findByIds")
    public List<Shop> findByIds (@RequestBody List<Long> ids) {
        System.out.println("GLI ID SONO QUESTI QUA");
        System.out.println(ids);
        return iShopRepository.findAllById(ids);
    }

    @RequestMapping(method = RequestMethod.GET, value="/shop/{shopId}")
    public Optional<Shop> findById (Long shopId) {
        return iShopRepository.findById(shopId);
    }

}
