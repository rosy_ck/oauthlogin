package com.tflogin.oauthlogin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Collections;

@SpringBootApplication
public class OauthloginApplication {

    public static void main(String[] args) {
        SpringApplication.run(OauthloginApplication.class, args);
    }

}
