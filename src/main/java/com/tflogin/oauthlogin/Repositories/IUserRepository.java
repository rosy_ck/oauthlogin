package com.tflogin.oauthlogin.Repositories;

import com.tflogin.oauthlogin.Entities.Shop;
import com.tflogin.oauthlogin.Entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface IUserRepository extends JpaRepository<User, Long> {
    @Query("select b from User b where b.email = ?1")
    User findByEmailAddress(String email);

    @Transactional
    @Modifying (clearAutomatically = true)
    @Query("update User b set b.telephone = ?1, b.role = ?2,b.birthDate=?3,b.fiscalCode=?4,b.address=?5,b.civicNumber=?6,b.city=?7 where b.id = ?8")
    int updateUser( String telephone,String role, String birthDate,String fiscalCode,   String address,  String civicNumber,  String city,Long id);

    @Transactional
    @Modifying (clearAutomatically = true)
    @Query("update User b set b.userShop = ?1 where b.email = ?2")
    int updateShop(Shop shop, String userEmail);

/*
@Transactional
@Modifying (clearAutomatically = true)
@Query("update Buyer b set b.telephone = :telephone where b.id = :id")
Void UpdateBuyer(@Param("telephone")String telephone,@Param("id") Long id);
 */
}
