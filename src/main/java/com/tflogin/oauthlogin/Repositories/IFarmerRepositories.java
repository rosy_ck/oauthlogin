package com.tflogin.oauthlogin.Repositories;

import com.tflogin.oauthlogin.Entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface IFarmerRepositories extends JpaRepository<User, Long> {
    @Query("select b from User b where b.role = 'farmer'")
    List<User> findAllFarmer();

    @Override
    Optional<User> findById(Long aLong);
}
