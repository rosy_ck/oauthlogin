package com.tflogin.oauthlogin.Repositories;

import com.tflogin.oauthlogin.Entities.Shop;
import com.tflogin.oauthlogin.Entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface IShopRepository extends JpaRepository<Shop, Long> {
    List<Shop> findById(long shopId);
}
