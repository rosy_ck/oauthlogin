FROM openjdk:15-alpine
RUN addgroup -S spring && adduser -S spring -G spring
VOLUME /tmp
EXPOSE 8085
ARG DEPENDENCY=target
ADD ${DEPENDENCY}/*.jar oauthlogin-2.4.3.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/oauthlogin-2.4.3.jar"]
CMD ["oauthloginapp", "serve", "--host", "0.0.0.0"]
